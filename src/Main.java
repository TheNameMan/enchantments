import javax.swing.*;

/**
 * @author TheNameMan
 */
public class Main {

    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, getEnchants(), "Enchantments", JOptionPane.INFORMATION_MESSAGE);
    }

    public static String getEnchants() {
        return "Enchantments:\n" +
                "\n" +
                "Armor;\n" +
                "\n" +
                "0 | minecraft:protection | Protection\n" +
                "1 | minecraft:fire_protection | Fire Protection\n" +
                "2 | minecraft:feather_falling | Feather Falling\n" +
                "3 | minecraft:blast_protection | Blast Protection\n" +
                "4 | minecraft:projectile_protection | Projectile Protection\n" +
                "5 | minecraft:respiration | Respiration\n" +
                "6 | minecraft:aqua_affinity | Aqua Affinity\n" +
                "7 | minecraft:thorns | Thorns\n" +
                "8 | minecraft:depth_strider | Depth Strider (1.8)\n" +
                "\n" +
                "34 | minecraft:unbreaking | Unbreaking\n" +
                "\n" +
                "Weapons;\n" +
                "\n" +
                "16 | minecraft:sharpness | Sharpness\n" +
                "17 | minecraft:smite | Smite\n" +
                "18 | minecraft:bane_of_arthropods | Bane Of Arthropods\n" +
                "19 | minecraft:knockback | Knockback\n" +
                "20 | minecraft:fire_aspect | Fire Aspect\n" +
                "21 | minecraft:looting | Looting\n" +
                "\n" +
                "34 | minecraft:unbreaking | Unbreaking\n" +
                "\n" +
                "Tools;\n" +
                "\n" +
                "32 | minecraft:efficiency | Efficiency\n" +
                "33 | minecraft:silk_touch | Silk Touch\n" +
                "34 | minecraft:unbreaking | Unbreaking\n" +
                "35 | minecraft:fortune | Fortune\n" +
                "\n" +
                "Bows;\n" +
                "\n" +
                "48 | minecraft:power | Power\n" +
                "49 | minecraft:punch | Punch\n" +
                "50 | minecraft:flame | Flame\n" +
                "51 | minecraft:infinity | Infinity\n" +
                "\n" +
                "Fishing Rods;\n" +
                "\n" +
                "61 | minecraft:luck_of_the_sea | Luck of the Sea (1.8)\n" +
                "62 | minecraft:lure | Lure (1.8)" +
                "\n" +
                "Credit: http://bitly.com/1q0kt61 - Feel free to use unshort.me to unshorten :)";
    }

}